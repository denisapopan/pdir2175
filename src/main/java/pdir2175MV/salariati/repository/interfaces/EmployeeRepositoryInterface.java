package pdir2175MV.salariati.repository.interfaces;

import java.util.List;

import pdir2175MV.salariati.enumeration.DidacticFunction;
import pdir2175MV.salariati.exception.EmployeeException;
import pdir2175MV.salariati.model.Employee;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee) throws EmployeeException;
	void modifyEmployee(String lastName, DidacticFunction function) throws Exception;
	List<Employee> getEmployeeList();
	List<Employee> ordoneazaDupaSalariu();
	List<Employee> ordoneazaDupaVarsta() throws EmployeeException;
	void deleteAllFromFile();
	String compareCNPs(String cnp1, String cnp2);


}
