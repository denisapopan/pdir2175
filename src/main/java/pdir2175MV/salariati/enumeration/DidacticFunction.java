package pdir2175MV.salariati.enumeration;

public enum DidacticFunction {
	ASISTENT, LECTURER, TEACHER;
}
