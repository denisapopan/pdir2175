package pdir2175MV.salariati.controller;

import java.util.ArrayList;
import java.util.List;

import pdir2175MV.salariati.enumeration.DidacticFunction;
import pdir2175MV.salariati.exception.EmployeeException;
import pdir2175MV.salariati.model.Employee;
import pdir2175MV.salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public void addEmployee(Employee employee) throws EmployeeException {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(String lastName, DidacticFunction function) throws Exception {
		employeeRepository.modifyEmployee(lastName, function);
	}

	public List<Employee> ordonezaDupaSalariu(){
		return employeeRepository.ordoneazaDupaSalariu();
	}

	public List<Employee> ordonezaDupaVarsta() throws EmployeeException{
		return  employeeRepository.ordoneazaDupaVarsta();
	}

	public void deleteAll() {
		employeeRepository.deleteAllFromFile();
	}

	public String compareCNPs(Employee e1,Employee e2){
		return employeeRepository.compareCNPs(e1.getCnp(),e2.getCnp());
	}

}
