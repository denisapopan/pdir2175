package pdir2175MV.salariati.main;

import pdir2175MV.salariati.exception.EmployeeException;
import pdir2175MV.salariati.model.Employee;
import pdir2175MV.salariati.repository.implementations.EmployeeRepository;
import pdir2175MV.salariati.repository.interfaces.EmployeeRepositoryInterface;
import pdir2175MV.salariati.controller.EmployeeController;
import pdir2175MV.salariati.enumeration.DidacticFunction;

import java.util.List;
import java.util.Scanner;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {

    static private EmployeeRepositoryInterface employeesRepository = new EmployeeRepository();
    static private EmployeeController employeeController = new EmployeeController(employeesRepository);

    public static void main(String[] args) {

        menu();

//		for(Employee _employee : employeeController.getEmployeesList())
//			System.out.println(_employee.toString());
//		System.out.println("-----------------------------------------");
//
//		Employee employee = new Employee("LastName", "1234567894321", DidacticFunction.ASISTENT, "2500");
//		employeeController.addEmployee(employee);
//
//		for(Employee _employee : employeeController.getEmployeesList())
//			System.out.println(_employee.toString());
//
//		EmployeeValidator validator = new EmployeeValidator();
//		System.out.println( validator.isValid(new Employee("LastName", "1234567894322", DidacticFunction.TEACHER, "3400")) );

    }

    private static void printMenu() {
        System.out.println("-----------------------------------------");
        System.out.println("1. Adauga un nou angajat.");
        System.out.println("2. Modifica functia didactica a unui angajat.");
        System.out.println("3. Afiseaza salariatii ordonati descrescator dupa salariu ");
        System.out.println("4. Afiseaza salariatii ordonati crescator dupa varsta ");
        System.out.println("5. Afiseaza angajatii.");
        System.out.println("6. Exit.");
        System.out.println("Introduceti comanda: ");
    }

    private static void menu() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            printMenu();
            switch (scanner.nextInt()) {
                case 1: {
                    try {
                        adaugaAngajat();
                    }
                    catch(EmployeeException e){}
                    break;
                }
                case 2: {
                    try {
                        modificaAngajat();
                    }
                    catch(Exception e){}
                    break;
                }
                case 3: {
                    ordoneazaDupaSalariu();
                    break;
                }
                case 4: {
                    try{
                        ordoneazaDupaVarsta();
                    }
                    catch(EmployeeException e){}
                    break;
                }
                case 5: {
                    getAllEmployees();
                    break;
                }
                case 6: return;
                default: {
                    System.out.println("Comanda invalida!");
                    printMenu();
                }
            }
        }
    }

    private static void adaugaAngajat() throws EmployeeException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nume: ");
        String lastName = scanner.next();
        System.out.println("CNP: ");
        String cnp = scanner.next();
        System.out.println("Functie: ");
        String function = scanner.next();
        System.out.println("Salariu: ");
        String salary = scanner.next();


        Employee employee = new Employee(lastName, cnp, null, salary);

        DidacticFunction didacticFunction = checkFunction(function);
        if(didacticFunction == null){
            System.out.println("Date invalide!!");
            return;
        }
        employee.setFunction(didacticFunction);
        employeeController.addEmployee(employee);
        System.out.println("Adaugat cu succes!");
    }

    private static DidacticFunction checkFunction(String function){
        if(function.equals("ASISTENT"))
            return DidacticFunction.ASISTENT;
        if(function.equals("LECTURER"))
            return DidacticFunction.LECTURER;
        if(function.equals("TEACHER"))
            return DidacticFunction.TEACHER;
        return null;
    }

    private static void modificaAngajat() throws Exception{
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nume: ");
        String lastName = scanner.next();
        System.out.println("Noua functie: ");
        String function = scanner.next();
        DidacticFunction didacticFunction = checkFunction(function);

        if(didacticFunction == null){
            System.out.println("Date invalide!!");
            return;
        }
        employeeController.modifyEmployee(lastName, didacticFunction);
        System.out.println("Modificat cu succes!");
    }

    private static void getAllEmployees(){
        List<Employee> employees = employeeController.getEmployeesList();
        for(Employee e : employees){
            System.out.println(e.toString());
        }
    }

    private static void ordoneazaDupaSalariu(){
        List<Employee> employees = employeeController.ordonezaDupaSalariu();
        for(Employee e : employees){
            System.out.println(e.toString());
        }

    }

    private static void ordoneazaDupaVarsta() throws EmployeeException{
        List<Employee> employees = employeeController.ordonezaDupaVarsta();
        for(Employee e : employees){
            System.out.println(e.toString());
        }

    }

}
