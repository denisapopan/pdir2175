package pdir2175MV;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pdir2175MV.salariati.controller.EmployeeController;
import pdir2175MV.salariati.enumeration.DidacticFunction;
import pdir2175MV.salariati.exception.EmployeeException;
import pdir2175MV.salariati.model.Employee;
import pdir2175MV.salariati.repository.implementations.EmployeeRepository;

import java.util.List;


public class EmployeeTest {

    private EmployeeController ctrl;
    private EmployeeRepository repo;

    @Before
    public void init() throws EmployeeException {

        repo = new EmployeeRepository();
        ctrl = new EmployeeController(repo);
        Employee firstEmployee = new Employee("Raluca", "2960312125678", DidacticFunction.LECTURER, "10000");
        Employee secondEmployee = new Employee("Larisa", "2930517125678", DidacticFunction.TEACHER, "10000");
        Employee thirdEmployee = new Employee("Marius", "1960311125678", DidacticFunction.LECTURER, "10000");
        ctrl.addEmployee(firstEmployee);
        ctrl.addEmployee(secondEmployee);
        ctrl.addEmployee(thirdEmployee);
    }

    @After
    public void after(){
        ctrl.deleteAll();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    @Test
    public void testECP1() throws EmployeeException {
        Employee employee = new Employee("Denisa", "2970917125805", DidacticFunction.TEACHER, "3000");
        ctrl.addEmployee(employee);
    }

    @Test(expected = EmployeeException.class)
    public void testECP2() throws EmployeeException {
        Employee employee = new Employee("123", "123", DidacticFunction.TEACHER, "2000");
        ctrl.addEmployee(employee);
    }


//    @Test
//    public void testECP3() throws EmployeeException {
//        Employee employee = new Employee("A", 123, DidacticFunction.TEACHER, "10");
//        ctrl.addEmployee(employee);
//    }

    @Test
    public void testBVA1() throws EmployeeException {
        int nrEmployees = ctrl.getEmployeesList().size();
        Employee employee = new Employee("ben", "2970917125805", DidacticFunction.TEACHER, "200");
        ctrl.addEmployee(employee);
        assert (ctrl.getEmployeesList().size() == nrEmployees + 1);
    }

    @Test(expected = EmployeeException.class)
    public void testBVA2() throws EmployeeException {
        Employee employee = new Employee("ben", "29709171258051", DidacticFunction.TEACHER, "200");
        ctrl.addEmployee(employee);
    }

    @Test(expected = EmployeeException.class)
    public void testBVA3() throws EmployeeException {
        Employee employee = new Employee("ben", "2970917125805", DidacticFunction.TEACHER, "0");
        ctrl.addEmployee(employee);
    }

    @Test
    public void testBVA4() throws EmployeeException {
        int nrEmployees = ctrl.getEmployeesList().size();
        Employee employee = new Employee("ben", "2970917125805", DidacticFunction.TEACHER, "1");
        ctrl.addEmployee(employee);
        assert (ctrl.getEmployeesList().size() == nrEmployees + 1);
    }


    //~~~~~~~~~~~~~~~~~LABORATOR 3~~~~~~~~~~~~~~~~~~~~~

    @Test(expected = Exception.class)
    public void testModifyEmployee1() throws Exception {
        List<Employee> firstList = ctrl.getEmployeesList();
        ctrl.modifyEmployee(null, DidacticFunction.TEACHER);
        List<Employee> secondList = ctrl.getEmployeesList();

        for (int i = 0; i < firstList.size(); i++) {
            assert (firstList.get(i).getFunction() == secondList.get(i).getFunction());
        }
    }


    @Test(expected = Exception.class)
    public void testModifyEmployee2() throws Exception {
        List<Employee> firstList = ctrl.getEmployeesList();
        ctrl.modifyEmployee("a", DidacticFunction.TEACHER);
        List<Employee> secondList = ctrl.getEmployeesList();

        for (int i = 0; i < firstList.size(); i++) {
            assert (firstList.get(i).getFunction() == secondList.get(i).getFunction());
        }
    }


    @Test
    public void testModifyEmployee3() throws Exception {
        List<Employee> firstList = ctrl.getEmployeesList();
        int size = firstList.size();
        ctrl.deleteAll();
        assert (ctrl.getEmployeesList().size() == 0);
        ctrl.modifyEmployee("ana", DidacticFunction.TEACHER);
        assert (ctrl.getEmployeesList().size() == 0);

        for (Employee e : firstList) {
            try {
                ctrl.addEmployee(e);
            } catch (EmployeeException ex) {
                System.out.println(ex);
            }
        }
        assert (ctrl.getEmployeesList().size() == size);
    }

    @Test
    public void testModifyEmployee4() throws Exception {
        List<Employee> firstList = ctrl.getEmployeesList();
        ctrl.modifyEmployee("maria", DidacticFunction.TEACHER);
        List<Employee> secondList = ctrl.getEmployeesList();

        for (int i = 0; i < firstList.size(); i++) {
            assert (firstList.get(i).getFunction() == secondList.get(i).getFunction());
        }
    }

    @Test
    public void testModifyEmployee5() throws Exception {
        List<Employee> firstList = ctrl.getEmployeesList();
        ctrl.modifyEmployee("alina", DidacticFunction.TEACHER);
        List<Employee> secondList = ctrl.getEmployeesList();

        for (int i = 0; i < firstList.size(); i++) {
            if (secondList.get(i).getLastName().equals("alina"))
                assert (firstList.get(i).getFunction() != secondList.get(i).getFunction());
        }

        ctrl.modifyEmployee("alina", DidacticFunction.ASISTENT);
    }

    //~~~~~~~~~~~~LABORATOR 4~~~~~~~~~~~~~~

    @Test
    public void testOrderByAge()throws EmployeeException {
        List<Employee> orderedEmployees = ctrl.ordonezaDupaVarsta();
        for (int i = 0; i < orderedEmployees.size()-1; i++){
            assert (ctrl.compareCNPs(orderedEmployees.get(i),orderedEmployees.get(i+1)) == orderedEmployees.get(i).getCnp());
        }
    }

    @Test(expected = EmployeeException.class)
    public void testOrderByAge2() throws EmployeeException{
        ctrl.deleteAll();
        List<Employee> result = ctrl.ordonezaDupaVarsta();
    }
}
